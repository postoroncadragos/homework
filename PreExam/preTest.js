let button = document.querySelector(".button");
let locat = document.querySelector(".inputValue");
let temp = document.querySelector(".temp");
let hum = document.querySelector(".hum");
let nume = document.querySelector(".name");
let clouds = document.querySelector(".clouds");
let wind = document.querySelector(".wind");
let rain = document.querySelector(".rain");
let image = document.querySelector("img");
let localTime = document.querySelector(".localHour");
let weatherIcon = document.querySelector(".weatherIcon");
let weatherText = document.querySelector(".weatherText");
let timeZoneDiff = document.querySelector(".list-element1");
let subscribeButton = document.querySelector(".subscribeBtn");
let em = document.querySelector(".emailInput");
let possibleMatch = document.querySelector(".possibleMatches");
let sugestions = document.querySelectorAll(".matches");




button.addEventListener("click", () => {
  fetch("https://weatherapi-com.p.rapidapi.com/forecast.json?q=" + locat.value + "&days=3", {
    "method": "GET",
    "headers": {
      "x-rapidapi-key": "2f1c6f7ff3msh56f8e16865e725cp1c6921jsna5ca256bd212",
      "x-rapidapi-host": "weatherapi-com.p.rapidapi.com"
    }
  })
    .then(res => res.json())
    .then(data => {
      let localT = data.location.localtime;
      let d = new Date(localT);
      let condition = data.current.condition["text"];
      weatherText.innerHTML = condition;     
      weatherIcon.innerHTML = `<img src="${data.current.condition["icon"]}">`;
      let hours = d.getHours();
      let minutes = d.getMinutes();
      const completeDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
      const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      let weekDay = completeDays[d.getDay()];
      let monthDate = d.getDate();
      let month = d.getMonth()
      let year = d.getFullYear()
      localTime.innerHTML = `${hours}:${minutes} - ${weekDay}, ${monthDate} ${months[month]} '${year % 100}`;
      let temperature = data.current["temp_c"];
      let cloudsData = data.current["cloud"];
      let rainData = data.current["precip_mm"];
      if (isNaN(rainData)) {
        rainData = 0;
      }
      clouds.innerHTML = cloudsData;
      nume.innerHTML = data.location["name"];
      hum.innerHTML = data.current['humidity'];
      wind.innerHTML = data.current["wind_mph"]
      rain.innerHTML = rainData;
      temp.innerHTML = Math.floor(temperature);
      if (hours >= 21 || hours <= 5) {
        image.src = "./images/night/" + condition + ".jpg";
      } else image.src = "./images/day/" + condition + ".jpg";


    })
    .catch(err => alert("Wrong city name!"))
})

locat.addEventListener('input', function () {

  fetch("https://wft-geo-db.p.rapidapi.com/v1/geo/cities?namePrefix=" + locat.value, {
    "method": "GET",
    "headers": {
      "x-rapidapi-key": "58838a9668mshef9526f4850f047p148e74jsn5a36ba8cfc79",
      "x-rapidapi-host": "wft-geo-db.p.rapidapi.com"
    }
  })
    .then(response => response.json())

    .then(response => {
      possibleMatch.style.visibility = "visible";

      for (i = 0; i < sugestions.length; i++) {
        let j = i;
        sugestions[i].textContent = response.data[i].city;
        sugestions[i].addEventListener("click", function () {
          locat.value = sugestions[j].innerHTML;
        })
      }

    })

    .catch(err => {
      console.error(err);
    });

});


subscribeButton.addEventListener("click", () => {
  fetch("http://157.230.238.199:1337/subscribe-tests", {
    method: "POST",
    body: JSON.stringify({
      email: em.value
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })

    .then(response => response.json())
    .catch(err => alert("Invalid Email"))
})

