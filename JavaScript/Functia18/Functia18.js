/*Write a real-valued function RadToDeg(R) that converts the angle value R in radians
 into the one in degrees (R is a real number, 0 ≤ R < 2·π). Note that 180° = π radians
  and use 3.14 for a value of π. Using this function, convert five given angles from radians into degrees.
 */
const RadToDeg = function (R) {
    if (R < 0 || R >= 2 * Math.PI) {
        return "Out of range";
    }
    const result = R * 180 / Math.PI;
    return result;
}


const solve = function () {
    let a = document.forma.sequence.value;
    let noCommas = a.replace(/,/g, ".");
    let arr = noCommas.split(";");
    let res = "";
    for (let i = 0; i < arr.length; i++) {
        res += arr[i] + " radiani = " + RadToDeg(arr[i]) + " grade; ";
    }
    document.getElementById("result").innerHTML = res;
}
