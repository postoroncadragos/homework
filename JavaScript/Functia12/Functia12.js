/*Write a logical function IsPowerN(K, N) that returns True, if an positive integer parameter K
  is equal to N (> 1) raised to some integer power, and False otherwise. Having input an integer N (> 1)
  and a sequence of 10 positive integers and using this function, find the amount of powers of base N
  in the given sequence. */
const IsPowerN = function (K, N) {
  if (K == 1) {
    return true;
  }
  while (K >= N) {
    K /= N;
  } return K == 1;
}

const solve = function () {
  function giveMeTheNumber(string) {
    return string.split(",")
      .map(x => { return IsPowerN(parseInt(x), N) ? 1 : 0 })
      .reduce((a, b) => { return a + b });
  }
  let N = document.forma.v.value;
  let a = document.forma.a.value;
  let res = "Din sirul de mai sus " + giveMeTheNumber(a) + " numere reprezinta puteri ale lui " + N;
  document.getElementById("result").innerHTML = res;
}