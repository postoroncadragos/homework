/* Write a function AddRightDigit(D, K) that adds a digit D to the right side of the decimal representation of a
    positive integer K and returns the obtained number (D and K are input integer parameters, the value of D is
    in the range 0 to 9). Having input an integer K and two one-digit numbers D1, D2 and using two calls of this
    function, sequentially add the given digits D1, D2 to the right side of the given K and output the result of
    each adding.
 */
const AddRightDigit = function (D, K) {
    if (parseInt(K) < 0) {
        return "-";
    }
    if (D > 10) {
        return "Give a digit";
    }
    let result = parseInt(K) * 10 + parseInt(D);
    K = result;
    return result;
}

const solve = function () {
    let D = document.forma.digit.value;
    let K = document.forma.number.value;
    let res = AddRightDigit(D, K);

    document.getElementById("num").value = res;
}
