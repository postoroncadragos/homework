/*Write a logical function IsPower5(K) that returns True, if an positive integer parameter K is
  equal to 5 raised to some integer power, and False otherwise. Using this function,
  find the amount of powers of base 5 in a given sequence of 10 positive integers. */
const IsPower5 = function (K) {
  if (K == 1) {
    return true
  }
  while (K >= 5) {
    K /= 5;
  } return K == 1;
}

const solve = function () {
  function giveMeTheNumber(string) {
    return string.split(",")
      .map(x => { return IsPower5(parseInt(x)) ? 1 : 0 })
      .reduce((a, b) => { return a + b });
  }
  let a = document.forma.a.value;
  let res = "Din sirul de mai sus " + giveMeTheNumber(a) + " numere reprezinta puteri in baza 5";
  document.getElementById("result").innerHTML = res;
}