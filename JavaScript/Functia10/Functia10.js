
/* Write a logical function IsSquare(K) that returns True, if an positive integer parameter K is a square of some integer, and False otherwise.
         Using this function, find the amount of squares in a given sequence of 10 positive integers. */
const IsSquare = function (K) {
  return Number.isInteger(Math.sqrt(K));
}

const solve = function () {
  let a = document.forma.a.value;
  let res;
  function giveMeTheNumber(string) {
    return string.split(",")
      .map(x => { return IsSquare(parseInt(x)) ? 1 : 0 })
      .reduce((a, b) => { return a + b });
  }
  res = "Ai introdus " + giveMeTheNumber(a) + " patrate perfecte";
  document.getElementById("result").innerHTML = res;
}

