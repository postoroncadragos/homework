/* Write an integer function Sign(X) that returns the following value:
        −1, if X < 0; 0, if X = 0; 1, if X > 0      
        (X is a real-valued parameter). Using this function, evaluate an expression Sign(A) + Sign(B) for given real numbers A and B. */
const Sign = function (x) {
    if (x < 0) {
        return -1;
    }
    if (x == 0) {
        return 0;
    } return 1;
}
const solve = function () {
    let A = document.Functia1.numberA.value;
    let B = document.Functia1.numberB.value;
    let SemnSuma = Sign(A) + Sign(B);
    let result = Sign(A) + " + " + "(" + Sign(B) + ")" + "=" + SemnSuma;
    document.getElementById("result").innerHTML = result;
}