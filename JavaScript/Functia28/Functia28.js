/* Write a function InvDigits(K) that inverts the order of digits of a positive integer K and returns the
    obtained integer (K is an input parameter). Using this function, invert the order of digits for each of five
    given integers.
 */
const InvDigits = function (K) {
    if (parseInt(K) < 0) {
        return "-";
    }
    let reverted = K.split("").reverse().join("");
    return reverted;
}

const solve = function () {
    let sequence = document.forma.sequence.value;
    let res = sequence.split(",").map(x => { return " Inversul  lui: " + x + " este " + InvDigits(x) + " " });
    document.getElementById("result").innerHTML = res;
}
