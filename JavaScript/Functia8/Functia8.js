/*Write an integer function Quarter(x, y) that returns the number of a coordinate quarter containing
 a point with nonzero real-valued coordinates (x, y). Using this function, find the numbers of coordinate
  quarters containing each of three points with given nonzero coordinates.
 */
const Quarter = function (x, y) {
    if (x == "" || y == "") {
        return ""
    } 
    if (x == 0 || y == 0) {
        return "onOXOY";
    } 
    if (x > 0) {
        return (y > 0) ? 1 : 2;
    } 
    
    return (y < 0) ? 3 : 4;
}
const solve = function () {
    let x1 = document.forma.x1.value;
    let y1 = document.forma.y1.value;
    let x2 = document.forma.x2.value;
    let y2 = document.forma.y2.value;
    let x3 = document.forma.x3.value;
    let y3 = document.forma.y3.value;
    let one = Quarter(x1, y1);
    let two = Quarter(x2, y2);
    let three = Quarter(x3, y3);
    function res(x) {
        if (x === "") {
            return "";
        }
        if (x === "onOXOY") {
            return x;
        }
        return "Cadranul " + x;
    }
    document.getElementById("result1").innerHTML = res(one);
    document.getElementById("result2").innerHTML = res(two);
    document.getElementById("result3").innerHTML = res(three);
}
