/*Write a real-valued function Fact2(N) that returns a double factorial N!!:

N!! = 1·3·5·…·N, if N is an odd number;
N!! = 2·4·6·…·N otherwise

(N is a positive integer; the real return type allows to avoid the integer overflow during the calculation
     of the double factorials for large values of N). Using this function, find the double factorials of five given integers.
 */
const Fact2 = function (N) {
    let res = 1;
    let startPoint = 1
    if (N % 2 === 0) {
        startPoint = 2
    } 
    for (let i = startPoint; i <= N; i += 2) {
        res *= parseInt(i);
    }
    
    return res;
}


const solve = function () {
    let a = document.forma.sequence.value;
    let res = a.split(",").map(x => { return "Double factorial of " + x + "=" + Fact2(x) + " " });
    document.getElementById("result").innerHTML = res;
}
