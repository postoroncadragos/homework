/* Write a function TrianglePS(a) that computes the perimeter P = 3·a and the area S = a^2·(3^1/2)/4 of an
    equilateral triangle with the side a and returns the result as two real numbers (a is an input real-valued
    parameter). Using this function, find the perimeters and the areas of three triangles with the given lengths
    of the sides.
 */
const TrianglePS = function (a) {
    let P = 3 * a;
    let S = Math.pow(a, 2) * Math.sqrt(3) / 4;
    return {
        perimeter: P,
        area: S
    }
}

const solve = function () {
    let A = document.forma.a.value;
    let res = "Perimetrul: " + TrianglePS(A).perimeter + "; Aria:" + TrianglePS(A).area;
    document.getElementById("result").innerHTML = res;
}
