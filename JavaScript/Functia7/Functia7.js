/* Write a real-valued function Calc(A, B, Op) that performs an arithmetic operation
  over nonzero real numbers A and B and returns the result value. An arithmetic operation is determined by
 integer parameter Op as follows: 1 — subtraction, 2 — multiplication, 3 — division, and addition otherwise.
Having input real numbers A, B and integers N1, N2, N3 and using this function, perform over given A,
B three operations determined by given N1, N2, N3. Output the result value of each operation.*/

const Calc = function (A, B, Op) {
  if (A == 0 || B == 0) {
    return "nonzero real numbers"
  }
  let res;
  switch (Op) {
    case "1": res = A - B;
      break;
    case "2": res = A * B;
      break;
    case "3": res = A / B;
      break;
    default: res = Number(A) + Number(B)
  }
  return res;
}
const solve1 = function () {
  let a = document.forma.a.value;
  let b = document.forma.b.value;
  let Op1 = document.forma.Op1.value;
  let Op2 = document.forma.Op2.value;
  let Op3 = document.forma.Op3.value;
  function sign(x) {
    return x == 1 ? "-" : x == 2 ? "*" : x == 3 ? "/" : "+";
  }
  function result(operation) {
    return a + sign(operation) + b + "=" + Calc(a, b, operation);
  }
  document.getElementById("result1").innerHTML = result(Op1);
  document.getElementById("result2").innerHTML = result(Op2);
  document.getElementById("result3").innerHTML = result(Op3);
}