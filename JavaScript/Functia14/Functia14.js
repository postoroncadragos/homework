/* Func14. Write an integer function DigitCount(K) that returns the amount of digits in the decimal 
representation of a positive integer K. Using this function, find the amount of digits for each of five
 given positive integers. */
const DigitCount = function (K) {
  if (K < 0) {
    return "Not a positive integer;"
  }
  if (K == "") {
    return 0;
  }
  let count = 1;
  while (K >= 10) {
    K = Math.floor(K / 10);
    count++;
  }
  return count;
}

const solve = function () {
  let a = document.getElementById("a").value;
  let b = document.getElementById("b").value;
  let c = document.getElementById("c").value;
  let d = document.getElementById("d").value;
  let e = document.getElementById("e").value;
  let arr = [a, b, c, d, e];
  let result = 'Numerele introduse au ';
  for (let i = 0; i < arr.length; i++) {
    result += "N" + parseInt(i + 1) + ": " + "  " + DigitCount(arr[i]) + " cifre; ";
  }
  document.getElementById("result").innerHTML = result;
}