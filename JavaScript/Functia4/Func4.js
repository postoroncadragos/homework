/* Write a real-valued function RingS(R1, R2) that returns the area of a ring bounded by a concentric circles
of radiuses R1 and R2 (R1 and R2 are real numbers, R1 > R2). Using this function, find the areas of three 
rings with given outer and inner radiuses. Note that the area of a circle of radius R can be found by  formula 
S = π·R^2. Use 3.14 for a value of π.*/
const RingS = function (r1, r2) {
    if (r1 < r2) {
        return "Introdu alte valori";
    }
    return Math.PI * (Math.pow(r1, 2) - Math.pow(r2, 2));
}
const solve = function () {
    let R1 = document.forma.R1A.value;
    let R2 = document.forma.R2A.value;
    document.getElementById("aria1").innerHTML = RingS(R1, R2);
}
