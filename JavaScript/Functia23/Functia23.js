/* Write a function PowerA234(A) that computes the second, the third, and the fourth degrees of a real number A
   and returns these degrees as three real numbers (A is an input parameter). Using this function, find the
   second, the third, and the fourth degrees of five given real numbers.
 */
const PowerA234 = function (A) {
    let powers = [2, 3, 4];
    let result = [];
    for (let i = 0; i < powers.length; i++) {
        result[i] = Math.pow(A, powers[i]);
    }
    return result;
}

const solve = function () {
    let a = document.forma.sequence.value;
    let noCommas = a.replace(/,/g, ".");
    function modifiedResult(y) {
        let ourFunc = PowerA234(y);
        let string = " puterea a ";
        let arrStrings = ["doua", "treia", "patra"]
        let res = "";
        for (let i = 0; i < ourFunc.length; i++) {
            res += string + arrStrings[i] + " = " + PowerA234(y)[i] + " ; ";
        }
        return res;
    }
    let res = noCommas.split(";").map(x => { return "Pentru " + x + modifiedResult(x) + " " }).join("");
    document.getElementById("result").innerHTML = res;
}
