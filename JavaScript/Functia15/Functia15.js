/*Write an integer function DigitN(K, N) that returns the N-th digit in the decimal representation 
of a positive integer K provided that the digits are numbered from right to left. If the amount of 
digits is less than N then the function returns −1. Using this function, output sequentially 1st, 2nd,
 3rd, 4th, 5th digit for each of five given positive integers K1, K2, …, K5.
 */
const DigitN = function (K, N) {
    let str = K.toString();
    if (N > str.length) {
        return -1;
    }
    let rev = str.split("").reverse().join("");
    let result = rev.charAt(N - 1);
    return result;

}


const solve = function () {
    let K = document.forma.K.value;
    let N = document.forma.N.value;
    let ord = { 1: "1st: ", 2: "2nd: ", 3: "3rd: " }
    function ordine(x) {
        if (x < 4) {
            return ord[x];
        }
        if (x >= 4) {
            return x + "th: ";
        }
    }
    let res = "De la dreapta spre stanga " + ordine(N) + DigitN(K, N);
    document.getElementById("result").innerHTML = res;
}
