/* Write a function RectPS(x1, y1, x2, y2) that computes and returns the perimeter P and the area S of a
    rectangle whose opposite vertices have coordinates (x1, y1) and (x2, y2) and sides are parallel to
    coordinate axes (x1, y1, x2, y2 are input real-valued parameters). Using this function, find the perimeters
    and the areas of three rectangles with the given opposite vertices.
 */
const RectPS = function (x1, y1, x2, y2) {
    let a = Math.abs(x1 - x2);
    let b = Math.abs(y1 - y2);
    let P = 2 * (a + b);
    let S = a * b;
    return {
        perimeter: P,
        area: S
    };
}

const solve = function () {
    let x1 = document.forma.x1.value;
    let y1 = document.forma.y1.value;
    let x2 = document.forma.x2.value;
    let y2 = document.forma.y2.value;
    const rs = RectPS(x1, y1, x2, y2);
    let res = "Perimetrul: " + rs.perimeter + "; Aria:" + rs.area;
    document.getElementById("result").innerHTML = res;
}
