/*Write a real-valued function DegToRad(D) that converts the angle value D in degrees into the one in radians
 (D is a real number, 0 ≤ D < 360). Note that 180° = π radians and use 3.14 for a value of π.
  Using this function, convert five given angles from degrees into radians.
 */
const DegToRad = function (D) {
    if (D < 0 || D >= 360) {
        return "Out of range";
    }
    const result = D * Math.PI / 180;
    return result;
}


const solve = function () {
    let a = document.forma.sequence.value;
    let arr = a.split(",");
    let res = "";
    for (let i = 0; i < arr.length; i++) {
        res += arr[i] + " grade = " + DegToRad(arr[i]) + " radiani; ";
    }
    document.getElementById("result").innerHTML = res;
}
