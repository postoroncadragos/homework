/* Write an integer function RootCount(A, B, C) that returns the amount of roots of the quadratic 
        equation A·x^2 + B·x + C = 0 (A, B, C are real-valued parameters, A ≠ 0). Using this function, 
        find the amount of roots for each of three quadratic equations with given coefficients. 
        Note that the amount of roots is determined by the value of a discriminant:
        D = B^2 − 4·A·C. */
const RootCount = function (A, B, C) {
  let delta = Math.pow(B, 2) - 4 * A * C;
  if (delta < 0) {
    return 0;
  } else if (delta == 0) {
    return 1;
  } else return 2;
};
const solve = function () {
  let a = document.form.numarA.value;
  let b = document.form.numarB.value;
  let c = document.form.numarC.value;
  let rad;
  RootCount(a, b, c) == 1 ? (rad = "radacina") : (rad = "radacini");
  let res = "Ecuatia " + a + "X^2 +" + b + "X+ " + c + " = 0 are " + RootCount(a, b, c) + " " + rad;
  document.getElementById("result1").innerHTML = res;
};
