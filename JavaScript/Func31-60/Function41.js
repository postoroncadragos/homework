/* Write a real-valued function Sin1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function sin(x) defined as follows:
sin(x) = x − x^3/(3!) + x^5/(5!) − … + (−1)^n·x^(2·n+1)/((2·n+1)!) + … .
Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function sin(x) at a given point x for six given ε. */
const { Power1 } = require("./Function37");
const { factorial } = require("./Function40");
const Sin1 = function (x, eps) {
    if (eps <= 0) {
        return "Negative epsilon error";
    }
    const newTerm = (n) => {
        let sign = Math.pow(-1, (n - 1) / 2);
        return sign * Power1(x, n) / factorial(n);
    }
    let res = 0;
    for (let i = 1; Math.abs(newTerm(i)) >= eps; i += 2) {

        res += newTerm(i);
    }
    const fix3 = (num) => {
        return num.toFixed(3);
    }
    return Number(fix3(res));
}
module.exports = { Sin1 };
