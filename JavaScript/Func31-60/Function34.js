/*Write a function SortDec3(X) that sorts the list X of three real-valued items
 in descending order (the function returns the None value). Using this function,
  sort each of two given lists X and Y. */
const SortDec3 = function (x) {
  let result = x.sort((a, b) => { return b - a });
  return result;
}
module.exports = { SortDec3 };