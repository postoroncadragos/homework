/* Write a real-valued function Sin1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function sin(x) defined as follows:
sin(x) = x − x^3/(3!) + x^5/(5!) − … + (−1)^n·x^(2·n+1)/((2·n+1)!) + … .
Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function sin(x) at a given point x for six given ε. */
const { Sin1 } = require("./Function41");
describe("functia 41", () => {
    describe("Sin1", () => {
        xit("x= 2, eps = 0.9", () => {
            const result = Sin1(2, 0.9);

            expect(result).toEqual(0.667);
        });
        xit("x= 2.4, eps = 0.6", () => {
            const result = Sin1(2.4, 0.6);

            expect(result).toEqual(0.76);
        });
        xit("x negative", () => {
            const result = Sin1(-4, 0.65);

            expect(result).toEqual(0.662);
        });
        xit("eps negative", () => {
            const result = Sin1(4, -2);

            expect(result).toEqual('Negative epsilon error');
        });
    });
});