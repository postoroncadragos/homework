/* Write a real-valued function Atan1(x, ε) (x and ε are real numbers, |x| < 1, ε > 0)
 that returns an approximate value of the function atan(x) defined as follows:

atan(x) = x − x^3/3 + x^5/5 − … + (−1)^n·x^(2·n+1)/(2·n+1) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function atan(x) at a given point x for six given ε. */
const { Atan } = require("./Function44");
describe("functia 44", () => {
    describe("Atan", () => {
        xit("x= 0.35, eps = 0.2", () => {
            const result = Atan(0.35, 0.2);

            expect(result).toEqual(0.35);
        });
        xit("x= -0.35, eps = 0.2", () => {
            const result = Atan(-0.35, 0.01);

            expect(result).toEqual(-0.336);
        });
        xit("eps negative", () => {
            const result = Atan(4, -2);

            expect(result).toEqual('Negative epsilon error');
        });
        xit("invalid x", () => {
            const result = Atan(4, 2);

            expect(result).toEqual("Invalid x");
        });
    });
});