/* Write a real-valued function Power4(x, a, ε) (x, a, ε are real numbers, |x| < 1, a, ε > 0)
 that returns an approximate value of the function (1 + x)a defined as:

(1 + x)^a = 1 + a·x + a·(a−1)·x^2/(2!) + … + a·(a−1)·…·(a−n+1)·x^n/(n!) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function (1 + x)a at a given point x
 for a given exponent a and six given ε.*/
const { Power1 } = require("./Function37");
const { factorial } = require("./Function40");
const power4 = function (x, a, eps) {
    if (eps <= 0) {
        return "Negative epsilon error";
    }
    if (Math.abs(x) >= 1) {
        return "Invalid x";
    }
    if (!Number.isInteger(a)) {
        return "Invalid a"
    }
    const newTerm = (n, a) => {
        if (n >= a) {
            return 0;
        }
        return (factorial(a) / factorial(a - n)) * Power1(x, n) / factorial(n);
    }
    let res = 0;
    for (let i = 0; Math.abs(newTerm(i, a)) >= eps; i++) {
        res += newTerm(i, a);
    }
    const fix3 = (num) => {
        return num.toFixed(3);
    }
    return Number(fix3(res));
}
module.exports = { power4 };
