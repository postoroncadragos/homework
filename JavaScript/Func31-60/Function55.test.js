/* Using the MonthDays function from the task Func53, write a function NextDate(D, M, Y)
 that changes a correct date, represented at the "day D, month number M, year Y" format,
  to a next one and returns new values of day, month, and year (all numbers are integers).
   Apply this function to three given dates and output resulting next ones. */
const { nextDate } = require("./Function55");
describe("functia 55", () => {
    describe("nextDate", () => {
        xit("28feb2020", () => {
            const result = nextDate(28, 2, 2020);

            expect(result).toEqual({ day: 29, month: 2, year: 2020 });
        });
        xit("29feb2020", () => {
            const result = nextDate(29, 2, 2020);

            expect(result).toEqual({ day: 1, month: 3, year: 2020 });
        });
        xit("31march2003", () => {
            const result = nextDate(31, 3, 2003);

            expect(result).toEqual({ day: 1, month: 4, year: 2003 });
        });
        xit("31december1999", () => {
            const result = nextDate(31, 12, 1999);

            expect(result).toEqual({ day: 1, month: 1, year: 2000 });
        });
        xit("14february2011", () => {
            const result = nextDate(14, 2, 2011);

            expect(result).toEqual({ day: 15, month: 2, year: 2011 });
        });
        xit("29feb2021", () => {
            const result = nextDate(29, 2, 2021);

            expect(result).toEqual("Unexistent date");
        });
        xit("invalid month", () => {
            const result = nextDate(32, -2, 2004);

            expect(result).toEqual("Invalid number of month");
        });
        xit("invalid year", () => {
            const result = nextDate(12, 5, -1);

            expect(result).toEqual("Invalid number of year");
        });
        xit("invalid date", () => {
            const result = nextDate(29, 2, 2100);

            expect(result).toEqual("Unexistent date");
        });
    });
});
