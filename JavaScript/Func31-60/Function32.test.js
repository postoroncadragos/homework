/* Write a function Minmax(X, I, J) that writes the minimal value of items X_I and X_J
 of a list X to the item X_I and writes the maximal value of these items to the item X_J
  (I and J are input integer parameters, the function returns the None value).
   Using four calls of this function, find the minimal value and the maximal value among four given real numbers.*/
const { minmax } = require("./Function32");
describe("functia 32", () => {
    describe("minmax", () => {
        let X;

        beforeEach(() => {
            X = [4, -3, -1, 24];
        })
        xit("X twofirst", () => {
            const result = minmax(X, 0, 1);

            expect(result).toEqual([-3, 4]);
        })
        xit("X twoLast", () => {
            const result = minmax(X, 2, 3);

            expect(result).toEqual([-1, 24]);
        })
        xit("X 2,3", () => {
            const result = minmax(X, 1, 2);

            expect(result).toEqual([-3, -1]);
        })
        xit("X 1,4", () => {
            const result = minmax(X, 0, 3);

            expect(result).toEqual([4, 24]);
        })
    })
})