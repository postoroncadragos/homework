/* Using the MonthDays function from the task Func53, write a function PrevDate(D, M, Y)
 that changes a correct date, represented at the "day D, month number M, year Y" format,
  to a previous one and returns new values of day, month, and year (all numbers are integers).
   Apply this function to three given dates and output resulting previous ones. */
const { prevDate } = require("./Function54");
describe("functia 54", () => {
    describe("prevDate", () => {
        xit("1st of March->29/02", () => {
            const result = prevDate(1, 3, 2020);

            expect(result).toEqual({ day: 29, month: 2, year: 2020 });
        });
        xit("betweenYears", () => {
            const result = prevDate(1, 1, 2019);

            expect(result).toEqual({ day: 31, month: 12, year: 2018 });
        });
        xit("normal date", () => {
            const result = prevDate(3, 3, 2005);

            expect(result).toEqual({ day: 2, month: 3, year: 2005 });
        });
        xit("betweenMonths(2)", () => {
            const result = prevDate(1, 2, 1999);

            expect(result).toEqual({ day: 31, month: 1, year: 1999 });
        });
        xit("betweenMonths(3)", () => {
            const result = prevDate(1, 5, 2011);

            expect(result).toEqual({ day: 30, month: 4, year: 2011 });
        });
        xit("betweenMonths(4)", () => {
            const result = prevDate(1, 3, 2011);

            expect(result).toEqual({ day: 28, month: 2, year: 2011 });
        });
        xit("invalid month", () => {
            const result = prevDate(32, -2, 2004);

            expect(result).toEqual("Invalid number of month");
        });
        xit("invalid year", () => {
            const result = prevDate(12, 5, -1);

            expect(result).toEqual("Invalid number of year");
        });
        xit("invalid date", () => {
            const result = prevDate(29, 2, 2100);

            expect(result).toEqual("Unexistent date");
        });
    });
});