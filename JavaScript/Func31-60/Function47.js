/* Using the GCD2 function from the task Func46, write a procedure Frac1(a, b, p, q),
 that simplifies a fraction a/b to the irreducible form p/q (a and b are input integer parameters,
 p and q are output integer parameters). The sign of a resulting fraction p/q is assigned to
its numerator, so q > 0. Using this procedure, find the numerator and the denominator for each
of irreducible fractions a/b + c/d, a/b + e/f, a/b + g/h provided that integers a, b, c, d, e, f, g, h are given. */
function gcd2(a, b) {
    /*if (a < 0 || b < 0) {
        return "Not positive integers!"
    }*/
    //acuma putem utiliza si numere negative
    if (b === 0) {
        return a;
    }
    return gcd2(b, a % b);
};
const frac1 = function (a, b) {
    if (b === 0) {
        return "Zero denominator error!";
    }
    if (b < 0) {
        a *= -1;
        b *= -1;
    }
    let p = a / Math.abs(gcd2(a, b));
    let q = b / Math.abs(gcd2(a, b));
    return {
        irreductibleNumerator: p,
        irreductibleDenominator: q
    }
}
module.exports = { frac1, gcd2 };
