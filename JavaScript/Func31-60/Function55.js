/* Using the MonthDays function from the task Func53, write a function NextDate(D, M, Y)
 that changes a correct date, represented at the "day D, month number M, year Y" format,
  to a next one and returns new values of day, month, and year (all numbers are integers).
   Apply this function to three given dates and output resulting next ones. */
const { monthDays } = require("./Function53");
const nextDate = (d, m, y) => {
    if (d > monthDays(m, y) || d < 1) {
        return "Unexistent date"
    }
    if (m < 1 || m > 12) {
        return "Invalid number of month";
    }
    if (y <= 0) {
        return "Invalid number of year";
    }
    if (d === monthDays(m, y)) {
        if (m === 12) {
            y++;
            m = 1;
            d = 1;
        } else {
            m++;
            d = 1;
        }
    } else {
        d++;
    }
    return {
        day: d,
        month: m,
        year: y
    }

}
module.exports = { nextDate };