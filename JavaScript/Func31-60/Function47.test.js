/* Using the GCD2 function from the task Func46, write a procedure Frac1(a, b, p, q),
 that simplifies a fraction a/b to the irreducible form p/q (a and b are input integer parameters,
 p and q are output integer parameters). The sign of a resulting fraction p/q is assigned to
its numerator, so q > 0. Using this procedure, find the numerator and the denominator for each
of irreducible fractions a/b + c/d, a/b + e/f, a/b + g/h provided that integers a, b, c, d, e, f, g, h are given. */
const { frac1 } = require("./Function47");
describe("functia 47", () => {
    describe("frac1", () => {
        xit("both nonzero positive", () => {
            const result = frac1(48, 45);

            expect(result).toEqual({ irreductibleNumerator: 16, irreductibleDenominator: 15 });
        });
        xit("b negative", () => {
            const result = frac1(4, -26);

            expect(result).toEqual({ irreductibleNumerator: - 2, irreductibleDenominator: 13 });
        });
        xit("a = zero", () => {
            const result = frac1(0, 24);

            expect(result).toEqual({ irreductibleNumerator: 0, irreductibleDenominator: 1 });
        });
        xit("a negative", () => {
            const result = frac1(-5, 25);

            expect(result).toEqual({ irreductibleNumerator: - 1, irreductibleDenominator: 5 });
        });
        xit("both negative", () => {
            const result = frac1(-36, -24);

            expect(result).toEqual({ irreductibleNumerator: 3, irreductibleDenominator: 2 });
        });
        xit("b===0", () => {
            const result = frac1(12, 0);

            expect(result).toEqual("Zero denominator error!");
        });
    });
});