/* Write a function ShiftLeft3(X) that performs a left cyclic shift of a list X of three real-valued items:
 the value of each item should be assigned to the previous item and the value of the first item 
 should be assigned to the last item (the function returns the None value). Using this function,
  perform the left cyclic shift for each of two given lists X and Y. */
const ShiftLeft3 = function (X) {
    let first = X.shift();
    X.push(first);
    return X;
}
module.exports = { ShiftLeft3 };
