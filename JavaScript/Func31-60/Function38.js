/* Write a real-valued function Power2(A, N) that returns the power A^N calculated by the following formulas:
A^0 = 1;
A^N = A·A·…·A (N factors), if N > 0;
A^N = 1/(A·A·…·A) (|N| factors), if N < 0
(A is a real-valued parameter, N is an integer parameter). Having input a real number A and integers
 K, L, M and using this function, find the powers A^K, A^L, A^M. */
const Power2 = function (A, N) {
    if (!Number.isInteger(N)) {
        return "Give an integer value!"
    }
    if (N >= 0) {
        return Math.pow(A, N);
    }
    return 1 / Math.pow(A, Math.abs(N));
}
module.exports = { Power2 };
