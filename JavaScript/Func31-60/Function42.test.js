/* Write a real-valued function Cos1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function cos(x) defined as follows:

cos(x) = 1 − x^2/(2!) + x^4/(4!) − … + (−1)^n·x^(2·n)/((2·n)!) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function cos(x) at a given point x for six given ε. */
const { Cos1 } = require("./Function42");
describe("functia 42", () => {
    describe("Cos1", () => {
        xit("x= 2, eps = 0.6", () => {
            const result = Cos1(2, 0.6);

            expect(result).toEqual(-0.333);
        });
        xit("x real", () => {
            const result = Cos1(2.2, 0.6);

            expect(result).toEqual(-0.444);
        });
        xit("x real negative", () => {
            const result = Cos1(-2.5, 0.75);

            expect(result).toEqual(-0.497);
        });
        xit("eps negative", () => {
            const result = Cos1(4, -2);

            expect(result).toEqual('Negative epsilon error');
        });
    });
});