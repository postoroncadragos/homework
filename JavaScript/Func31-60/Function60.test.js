/* Using the Dist function from the task Func59, write a function Alts(x_A, y_A, x_B, y_B, x_C, y_C)
that evaluates and returns the altitudes h_A, h_B, h_C drawn from the vertices A, B, C of a triangle ABC
(the coordinates of vertices are input real-valued parameters). Using this function,
 evaluate the altitudes of each of triangles ABC, ABD, ACD provided that the coordinates
of points A, B, C, D are given. */
const { alts } = require("./Function60");
describe("functia 60", () => {
    describe("alts", () => {
        xit("valid abc", () => {
            const result = alts(1, 4, 1, 1, 4, 1);

            expect(result).toEqual({ hAB: 3.001, hCB: 3.001, hAC: 2.122 });
        });
        xit("valid abc(2)", () => {
            const result = alts(2, -4, 1, -11, 4, 1);

            expect(result).toEqual({ hAB: 1.276, hCB: 0.73, hAC: 1.676 });
        });
        xit("valid abc(3)", () => {
            const result = alts(-1, -4, 1.5, 1, -3, 1);

            expect(result).toEqual({ hAB: 4.025, hCB: 5, hAC: 4.178 });
        });
        xit("the triangle is a line", () => {
            const result = alts(1, 1, 1, 1, 4, 1);

            expect(result).toEqual({ hAB: "Not a valid segment", hCB: 0, hAC: 0 });
        });
        xit("the triangle is a point", () => {
            const result = alts(3, 3, 3, 3, 3, 3);

            expect(result).toEqual({ hAB: "Not a valid segment", hCB: "Not a valid segment", hAC: "Not a valid segment" });
        });

    });
});