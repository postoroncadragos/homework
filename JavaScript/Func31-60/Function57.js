/* Using the Leng function from the task Func56, write a real-valued function
 Perim(x_A, y_A, x_B, y_B, x_C, y_C) that returns the perimeter of a triangle ABC
  with given coordinates of its vertices (x_A, y_A, x_B, y_B, x_C, y_C are real-valued parameters).
   Using the Perim function, find the perimeters of triangles ABC, ABD, ACD provided that 
   coordinates of points A, B, C, D are given.*/
const { leng } = require("./Function56");
//const checkTriangle
const perim = (xa, ya, xb, yb, xc, yc) => {
    const ab = leng(xa, ya, xb, yb);
    const ac = leng(xa, ya, xc, yc);
    const bc = leng(xb, yb, xc, yc);
    if (ab * ac * bc === 0) {
        return "Not a triangle"
    }
    let perimeter = ab + ac + bc;
    if (!Number.isInteger(perimeter)) {
        perimeter = Number(perimeter.toFixed(3));
    }
    return perimeter;
}
module.exports = { perim };