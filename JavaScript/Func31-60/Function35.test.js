/* Write a function ShiftRight3(X) that performs a right cyclic shift of a list X 
of three real-valued items: the value of each item should be assigned to the next 
item and the value of the last item should be assigned to the first item (the function 
returns the None value). Using this function, perform the right cyclic shift for each
 of two given lists X and Y. */
const { ShiftRight3 } = require("./Function35");
describe("functia 35", () => {
    describe("ShiftRight3", () => {
        xit("Shift list X", () => {
            let X = [3, 6, 4];

            const result = ShiftRight3(X);

            expect(result).toEqual([4, 3, 6]);
        });
        xit("Shift list Y", () => {
            let Y = [45, -56, -2];

            const result = ShiftRight3(Y);

            expect(result).toEqual([-2, 45, -56]);
        });
    });
});