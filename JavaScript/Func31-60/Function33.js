/*Write a function SortInc3(X) that sorts the list X of three real-valued items
 in ascending order (the function returns the None value). Using this function,
  sort each of two given lists X and Y. */
const SortInc3 = function (X) {
    let result = X.sort((a, b) => { return a - b });
    return result;
}
module.exports = { SortInc3 };