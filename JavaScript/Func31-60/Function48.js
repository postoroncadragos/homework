/* Taking into account that the least common multiple of two positive integers A and B equals A·(B/GCD(A, B)),
 where GCD(A, B) is the greatest common divisor of A and B, and using the GCD2 function from the task Func46,
  write an integer function LCM2(A, B) that returns the least common multiple of A and B. Using this function,
   find the least common multiple for each of pairs (A, B), (A, C), (A, D) provided that integers A, B, C, D are given. */
const { gcd2 } = require("./Function46");
const lcm2 = (a, b) => {
    if (a < 0 || b < 0) {
        return gcd2(a, b);
    }
    if (a * b === 0) {
        return "Nedefinit!"
    }
    return a * (b / (gcd2(a, b)));
}
module.exports = { lcm2 };