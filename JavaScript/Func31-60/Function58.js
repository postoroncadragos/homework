/* Using the Leng and Perim functions from the tasks Func56 and Func57, write a real-valued
 function Area(x_A, y_A, x_B, y_B, x_C, y_C) that returns the area of a triangle ABC:

S_ABC = (p·(p−|AB|)·(p−|AC|)·(p−|BC|))^1/2,

where p is the half-perimeter. Using the Area function, find the areas of triangles
 ABC, ABD, ACD provided that coordinates of points A, B, C, D are given. */
const { leng } = require("./Function56");
const { perim } = require("./Function57");
const area = (xa, ya, xb, yb, xc, yc) => {
    const p = perim(xa, ya, xb, yb, xc, yc) / 2;
    const ab = leng(xa, ya, xb, yb);
    const ac = leng(xa, ya, xc, yc);
    const bc = leng(xb, yb, xc, yc);
    if (ab * ac * bc === 0) {
        return perim(xa, ya, xb, yb, xc, yc)
    }
    let s = Math.sqrt(p * (p - ab) * (p - ac) * (p - bc));

    if (!Number.isInteger(s)) {
        s = Number(s.toFixed(3));
    }
    return s;
}
module.exports = { area };