/* Write a real-valued function Leng(x_A, y_A, x_B, y_B) that returns the length
 of a segment AB with given coordinates of its endpoints:

|AB| = ((x_A − x_B)^2 + (y_A − y_B)^2)^1/2

(x_A, y_A, x_B, y_B are real-valued parameters). Using this function,
 find the lengths of segments AB, AC, AD provided that coordinates of points A, B, C, D are given. */
const leng = (xa, ya, xb, yb) => {
    let aCatena = xa - xb;
    let bCatena = ya - yb;
    const segmLen = Math.sqrt(Math.pow(aCatena, 2) + Math.pow(bCatena, 2))

    return segmLen
}
module.exports = { leng };