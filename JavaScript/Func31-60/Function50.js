/* Write a function TimeToHMS(T) that converts a time interval T (in seconds)
 into the "hours H, minutes M, seconds S" format and returns the values H, M, S (T, H, M, S are integers).
  Using this function, find the amount of hours, minutes and seconds for each of five given time intervals
   T_1, T_2, …, T_5.  */
const TimeToHMS = (t) => {
    const absTime = Math.abs(t);
    const hours = (time) => {
        return Math.floor(time / 3600);
    }
    const minutes = (time) => {
        return Math.floor((time % 3600) / 60);
    }
    const seconds = (time) => {
        return Math.floor(time % 60);
    }
    let h = hours(absTime);
    let m = minutes(absTime);
    let s = seconds(absTime);
    if (t >= 0) {
        return {
            hours: h,
            minutes: m,
            seconds: s
        }
    }
    return {
        hours: -h,
        minutes: -m,
        seconds: -s
    }
}
module.exports = { TimeToHMS };