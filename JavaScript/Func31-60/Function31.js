/* Write a function Swap(X, I, J) that exchanges the values of items X_I and X_J
 of a list X of real numbers (I and J are input integer parameters, the function returns the None value).
 Having input a list of four real numbers and using three calls of this function,
 sequentially exchange the values of the two first, two last, and two middle items of the given list.
 Output the new values of the list. */
const swap = (X, I, J) => {
    let container = X[I - 1];
    X[I - 1] = X[J - 1];
    X[J - 1] = container;
    return X;
}

module.exports = { swap };