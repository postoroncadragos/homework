/* Taking into account the formula GCD(A, B, C) = GCD(GCD(A, B), C) and using the GCD2 function
 from the task Func46, write an integer function GCD3(A, B, C) that returns the greatest common 
 divisor of three positive integers A, B, C. Using this function, find the greatest common divisor 
 for each of triples (A, B, C), (A, C, D), (B, C, D) provided that integers A, B, C, D are given. */
const { gcd } = require("./Function49");
describe("functia 49", () => {
    describe("gcd", () => {
        xit("a b si c >0", () => {
            const result = gcd(36, 24, 16);

            expect(result).toEqual(4);
        });
        xit("a b si c >0(2)", () => {
            const result = gcd(23, 18, 69);

            expect(result).toEqual(1);
        });
        xit("b===0", () => {
            const result = gcd(48, 0, 12);

            expect(result).toEqual("Nedefinit!");
        });
        xit("a===0", () => {
            const result = gcd(0, 35, 21);

            expect(result).toEqual("Nedefinit!");
        });
        xit("c===0", () => {
            const result = gcd(35, 21, 0);

            expect(result).toEqual("Nedefinit!");
        });
        xit("one negative value", () => {
            const result = gcd(-2, 4, 67);

            expect(result).toEqual("Not positive integers!");
        });
        xit("two negative values", () => {
            const result = gcd(-22, 34, -4);

            expect(result).toEqual("Not positive integers!");
        });
        xit("three negative", () => {
            const result = gcd(-2, -54, -54);

            expect(result).toEqual("Not positive integers!");
        });
    });
});