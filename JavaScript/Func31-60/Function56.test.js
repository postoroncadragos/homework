/* Write a real-valued function Leng(x_A, y_A, x_B, y_B) that returns the length
 of a segment AB with given coordinates of its endpoints:

|AB| = ((x_A − x_B)^2 + (y_A − y_B)^2)^1/2

(x_A, y_A, x_B, y_B are real-valued parameters). Using this function,
 find the lengths of segments AB, AC, AD provided that coordinates of points A, B, C, D are given. */
const { leng } = require("./Function56");
describe("functia 56", () => {
    describe("leng", () => {
        xit("diff points a and b parallel ox", () => {
            const result = leng(2, 2, 5, 2);

            expect(result).toEqual(3);
        });
        xit("diff points a and b parallel oy", () => {
            const result = leng(1, 2, 1, 6);

            expect(result).toEqual(4);
        });
        xit("diff points a < b ", () => {
            const result = leng(1, 0, 4, 4);

            expect(result).toEqual(5);
        });
        xit("diff points a > b ", () => {
            const result = leng(4, 4, 1, 0);

            expect(result).toEqual(5);
        });
        xit("same points a and b", () => {
            const result = leng(1, 2, 1, 2);

            expect(result).toEqual(0);
        });
    });
});