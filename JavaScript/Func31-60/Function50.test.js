/* Write a function TimeToHMS(T) that converts a time interval T (in seconds)
 into the "hours H, minutes M, seconds S" format and returns the values H, M, S (T, H, M, S are integers).
  Using this function, find the amount of hours, minutes and seconds for each of five given time intervals
   T_1, T_2, …, T_5.  */
const { TimeToHMS } = require("./Function50");
describe("functia 50", () => {
    describe("TimeToHMS", () => {
        xit("time >0", () => {
            const result = TimeToHMS(60);

            expect(result).toEqual({ hours: 0, minutes: 1, seconds: 0 });
        });
        xit("time >0(2)", () => {
            const result = TimeToHMS(69);

            expect(result).toEqual({ hours: 0, minutes: 1, seconds: 9 });
        });
        xit("time >0(3)", () => {
            const result = TimeToHMS(3600);

            expect(result).toEqual({ hours: 1, minutes: 0, seconds: 0 });
        });
        xit("time >0(4)", () => {
            const result = TimeToHMS(3601);

            expect(result).toEqual({ hours: 1, minutes: 0, seconds: 1 });
        });
        xit("time >0(5)", () => {
            const result = TimeToHMS(3660);

            expect(result).toEqual({ hours: 1, minutes: 1, seconds: 0 });
        });
        xit("time >0(6)", () => {
            const result = TimeToHMS(7259);

            expect(result).toEqual({ hours: 2, minutes: 0, seconds: 59 });
        });
        xit("time >0(7)", () => {
            const result = TimeToHMS(-3601);

            expect(result).toEqual({ hours: -1, minutes: -0, seconds: -1 });
        });
    });
});