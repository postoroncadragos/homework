/* Using the Dist function from the task Func59, write a function Alts(x_A, y_A, x_B, y_B, x_C, y_C)
 that evaluates and returns the altitudes h_A, h_B, h_C drawn from the vertices A, B, C of a triangle ABC
  (the coordinates of vertices are input real-valued parameters). Using this function,
   evaluate the altitudes of each of triangles ABC, ABD, ACD provided that the coordinates
 of points A, B, C, D are given. */
const { dist } = require("./Function59");
const alts = (xa, ya, xb, yb, xc, yc) => {
    let hOnab = dist(xc, yc, xa, ya, xb, yb);
    let hOnbc = dist(xa, ya, xb, yb, xc, yc);
    let hOnac = dist(xb, yb, xa, ya, xc, yc);
    return {
        hAB: hOnab,
        hCB: hOnbc,
        hAC: hOnac
    }

}
module.exports = { alts };