/* Using the Leng and Area functions from the tasks Func56 and Func58,
 write a real-valued function Dist(x_P, y_P, x_A, y_A, x_B, y_B) that 
 returns the distance D(P, AB) between a point P and a line AB:

D(P, AB) = 2·S_PAB/|AB|,

where S_PAB is the area of the triangle PAB. Using this function,
 find the distance between a point P and each of lines AB, AC, BC provided 
 that coordinates of points P, A, B, C are given. */
const { dist } = require("./Function59");
describe("functia 59", () => {
    describe("dist", () => {
        xit("p,a,b diff points", () => {
            const result = dist(2, 2, 0, 0, 2, 0);

            expect(result).toEqual(1.999);
        });
        xit("p,a,b diff points,ab extra", () => {
            const result = dist(2, 2, -1, 0, 1, 0);

            expect(result).toEqual(2.001);//2
        });
        xit("p on ab", () => {
            const result = dist(3, 1, 5, 0, 1, 2);

            expect(result).toEqual(0);
        });
        xit("ab===0", () => {
            const result = dist(2, 3, 2, 2, 2, 2);

            expect(result).toEqual("Not a valid segment");
        });

    });
});