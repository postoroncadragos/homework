/* Taking into account the formula GCD(A, B, C) = GCD(GCD(A, B), C) and using the GCD2 function
 from the task Func46, write an integer function GCD3(A, B, C) that returns the greatest common 
 divisor of three positive integers A, B, C. Using this function, find the greatest common divisor 
 for each of triples (A, B, C), (A, C, D), (B, C, D) provided that integers A, B, C, D are given. */
const { gcd2 } = require("./Function46");
const gcd = (a, b, c) => {
    if (a < 0 || b < 0 || c < 0) {
        return gcd2(a, b);
    }
    if (a * b * c === 0) {
        return "Nedefinit!"
    }
    return gcd2(gcd2(a, b), c);
}
module.exports = { gcd };