/* Write a function Minmax(X, I, J) that writes the minimal value of items X_I and X_J
 of a list X to the item X_I and writes the maximal value of these items to the item X_J
 (I and J are input integer parameters, the function returns the None value).
 Using four calls of this function, find the minimal value and the maximal value among four given real numbers.*/
const minmax = (X, I, J) => {
    min = Math.min(X[I], X[J]);
    max = Math.max(X[I], X[J]);
    return [min, max];
}

module.exports = { minmax };