/* Write a function IncTime(H, M, S, T) that increases a time interval in hours H,
 minutes M, seconds S on T seconds and returns new values of hours, minutes,
  and seconds (all numbers are positive integers). Having input hours H, minutes M,
   seconds S (as integers) and an integer T and using the IncTime function,
    increase the given time interval on T seconds and output new values of H, M, S. */
const { TimeToHMS } = require("./Function50");
const incTime = function (h, m, s, t) {
    if (h < 0 || m < 0 || s < 0 || t < 0) {
        return "Give positive values!"
    }
    let hourSec = h * 3600;
    let minuteSec = m * 60;
    let actual = hourSec + minuteSec + s;
    let newTime = actual + t;
    let result = TimeToHMS(newTime);
    return result;
}
module.exports = { incTime };
