/*Write a function SortDec3(X) that sorts the list X of three real-valued items
 in descending order (the function returns the None value). Using this function,
  sort each of two given lists X and Y. */
const { SortDec3 } = require("./Function34");
describe("functia 34", () => {
    describe("SortDec3", () => {
        xit("Sort list x", () => {
            let x = [3, 6, 4];

            const result = SortDec3(x);

            expect(result).toEqual([6, 4, 3]);
        });
        xit("Sort list y", () => {
            let y = [45, -56, -2];

            const result = SortDec3(y);

            expect(result).toEqual([45, -2, -56]);
        });
    });
});