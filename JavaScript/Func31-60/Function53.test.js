/* Using the IsLeapYear function from the task Func52, write an integer function MonthDays(M, Y)
 that returns the amount of days for M-th month of year Y (M and Y are integers, 1 ≤ M ≤ 12, Y > 0).
  Output the return value of this function for a given year Y and each of given months M_1, M_2, M_3. */
const { monthDays } = require("./Function53");
describe("functia 53", () => {
    describe("monthDays", () => {
        xit("February of leap", () => {
            const result = monthDays(2, 2020);

            expect(result).toEqual(29);
        });
        xit("February not leap", () => {
            const result = monthDays(2, 2019);

            expect(result).toEqual(28);
        });
        xit("March 2005", () => {
            const result = monthDays(3, 2005);

            expect(result).toEqual(31);
        });
        xit("September 1999", () => {
            const result = monthDays(9, 1999);

            expect(result).toEqual(30);
        });
        xit("December 20", () => {
            const result = monthDays(12, 20);

            expect(result).toEqual(31);
        });
        xit("invalid month", () => {
            const result = monthDays(-2, 2004);

            expect(result).toEqual("Invalid number of month");
        });
        xit("invalid year", () => {
            const result = monthDays(12, -1);

            expect(result).toEqual("Invalid number of year");
        });
    });
});