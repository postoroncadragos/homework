/* Write a logical function IsLeapYear(Y) that returns True if a year Y 
(a positive integer parameter) is a leap year, and False otherwise.
 Output the return values of this function for five given values of the parameter Y.
  Note that a year is a leap year if it is divisible by 4 except for years that are 
  divisible by 100 and are not divisible by 400 */
const { isLeapYear } = require("./Function52");
describe("Functia 52", () => {
    describe("isLeapYear", () => {
        xit("divisible by4", () => {
            const res = isLeapYear(2020);

            expect(res).toEqual(true);
        });
        xit("divisible by 100 and not 400", () => {
            const res = isLeapYear(2100);

            expect(res).toEqual(false);
        });
        xit("divisible by 400", () => {
            const res = isLeapYear(2000);

            expect(res).toEqual(true);
        });
        xit("y not divisible by 4", () => {
            const res = isLeapYear(2021);

            expect(res).toEqual(false);
        });
    });

});