/* Write a real-valued function Power1(A, B) that returns the power A^B 
calculated by the formula A^B = exp(B·ln(A)) (A and B are real-valued parameters).
 In the case of zero-valued or negative parameter A the function returns 0.
  Having input real numbers P, A, B, C and using this function, find the powers A^P, B^P, C^P. */
const Power1 = function (A, B) {
  //let result = Math.exp(B * Math.log(A));
  let result = Math.pow(A, B);
  return result;
}
module.exports = { Power1 };
