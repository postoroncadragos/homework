/* Using the MonthDays function from the task Func53, write a function PrevDate(D, M, Y)
 that changes a correct date, represented at the "day D, month number M, year Y" format,
  to a previous one and returns new values of day, month, and year (all numbers are integers).
   Apply this function to three given dates and output resulting previous ones. */
const { monthDays } = require("./Function53");
const prevDate = (d, m, y) => {
    if (d > monthDays(m, y) || d < 1) {
        return "Unexistent date"
    }
    if (m < 1 || m > 12) {
        return "Invalid number of month";
    }
    if (y <= 0) {
        return "Invalid number of year";
    }
    if (d === 1) {
        if (m === 1) {
            y--;
            m = 12;
            d = monthDays(12, y)
        } else {
            m--;
            d = monthDays(m, y);
        }
    } else {
        d--;
    }
    return {
        day: d,
        month: m,
        year: y
    }

}
module.exports = { prevDate };