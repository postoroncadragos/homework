/* Write a function ShiftLeft3(X) that performs a left cyclic shift of a list X of three real-valued items:
 the value of each item should be assigned to the previous item and the value of the first item 
 should be assigned to the last item (the function returns the None value). Using this function,
  perform the left cyclic shift for each of two given lists X and Y. */
const { ShiftLeft3 } = require("./Function36");
describe("functia 36", () => {
    describe("ShiftLeft3", () => {
        xit("Shift list X", () => {
            let X = [3, 6, 4];

            const result = ShiftLeft3(X);

            expect(result).toEqual([6, 4, 3]);
        });
        xit("Shift list Y", () => {
            let Y = [45, -56, -2];

            const result = ShiftLeft3(Y);

            expect(result).toEqual([-56, -2, 45]);
        });
    });
});