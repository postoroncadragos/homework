/* Write an integer function GCD2(A, B) that returns the greatest common divisor (GCD)
 of two positive integers A and B. Use the Euclidean algorithm:

GCD(A, B) = GCD(B, A mod B), if B ≠ 0; GCD(A, 0) = A,

where "mod" denotes the operator of taking the remainder after integer division.
 Using this function, find the greatest common divisor for each of pairs (A, B), (A, C), (A, D)
  provided that integers A, B, C, D are given. */
function gcd2(a, b) {
    if (a < 0 || b < 0) {
        return "Not positive integers!"
    }
    if (b === 0) {
        return a;
    }
    return gcd2(b, a % b);
};
module.exports = { gcd2 };