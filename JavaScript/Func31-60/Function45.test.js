/* Write a real-valued function Power4(x, a, ε) (x, a, ε are real numbers, |x| < 1, a, ε > 0)
 that returns an approximate value of the function (1 + x)a defined as:

(1 + x)^a = 1 + a·x + a·(a−1)·x^2/(2!) + … + a·(a−1)·…·(a−n+1)·x^n/(n!) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function (1 + x)a at a given point x
 for a given exponent a and six given ε.*/
const { power4 } = require("./Function45");
describe("functia 44", () => {
    describe("power4", () => {
        xit("valid(1)", () => {
            const result = power4(0.2, 12, 0.4);

            expect(result).toEqual(8.592);
        });
        xit("valid(2)", () => {
            const result = power4(0.2, 12, 0.5);

            expect(result).toEqual(8.592);
        });
        xit("valid(3)", () => {
            const result = power4(0.2, 15, 0.3);

            expect(result).toEqual(15.305);
        });
        xit("invalid a", () => {
            const result = power4(0.9, 4.65, 0.2);

            expect(result).toEqual("Invalid a");
        });
        xit("invalid x", () => {
            const result = power4(12, 5, 0.9);

            expect(result).toEqual("Invalid x");
        });
        xit("x,a,neg eps", () => {
            const result = power4(0.3, 4, -0.4);

            expect(result).toEqual("Negative epsilon error");
        });

    });
});