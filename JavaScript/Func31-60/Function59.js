/* Using the Leng and Area functions from the tasks Func56 and Func58,
 write a real-valued function Dist(x_P, y_P, x_A, y_A, x_B, y_B) that 
 returns the distance D(P, AB) between a point P and a line AB:

D(P, AB) = 2·S_PAB/|AB|,

where S_PAB is the area of the triangle PAB. Using this function,
 find the distance between a point P and each of lines AB, AC, BC provided 
 that coordinates of points P, A, B, C are given. */
const { leng } = require("./Function56");
const { area } = require("./Function58");
const dist = (xp, yp, xa, ya, xb, yb) => {
    let s = area(xp, yp, xa, ya, xb, yb);
    let segmAB = leng(xa, ya, xb, yb);
    if (segmAB == 0) {
        return "Not a valid segment";
    }
    if (isNaN(s)) {
        return 0;
    }

    let distance = 2 * s / segmAB;
    if (!Number.isInteger(distance)) {
        distance = Number(distance.toFixed(3));
    }

    return distance;

}
module.exports = { dist };