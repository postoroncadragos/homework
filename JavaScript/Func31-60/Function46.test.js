/* Write an integer function GCD2(A, B) that returns the greatest common divisor (GCD)
 of two positive integers A and B. Use the Euclidean algorithm:

GCD(A, B) = GCD(B, A mod B), if B ≠ 0; GCD(A, 0) = A,

where "mod" denotes the operator of taking the remainder after integer division.
 Using this function, find the greatest common divisor for each of pairs (A, B), (A, C), (A, D)
  provided that integers A, B, C, D are given. */
const { gcd2 } = require("./Function46");
describe("functia 46", () => {
    describe("gcd2", () => {
        xit("b===0", () => {
            const result = gcd2(43, 0);

            expect(result).toEqual(43);
        });
        xit("a===0", () => {
            const result = gcd2(0, 24);

            expect(result).toEqual(24);
        });
        xit("valid1", () => {
            const result = gcd2(36, 24);

            expect(result).toEqual(12);
        });
        xit("valid2", () => {
            const result = gcd2(12, 69);

            expect(result).toEqual(3);
        });
        xit("a or b is negative", () => {
            const result = gcd2(-2, 34);

            expect(result).toEqual("Not positive integers!");
        });
    });
});