/* Write a real-valued function Power1(A, B) that returns the power A^B 
calculated by the formula A^B = exp(B·ln(A)) (A and B are real-valued parameters).
 In the case of zero-valued or negative parameter A the function returns 0.
  Having input real numbers P, A, B, C and using this function, find the powers A^P, B^P, C^P. */
const { Power1 } = require("./Function37");
describe("functia 37", () => {
    describe("Power1", () => {
        xit("Power2^4", () => {
            const result = Power1(2, 4);

            expect(result).toEqual(16);
        });
        xit("Power3^1", () => {
            const result = Power1(3, 1);

            expect(result).toEqual(3);
        });
        xit("Power-2^4", () => {
            const result = Power1(-2, 4);

            expect(result).toEqual(16);
        });
        xit("Power16^-0.5", () => {
            const result = Power1(16, -0.5);

            expect(result).toEqual(0.25);
        });
    });
});