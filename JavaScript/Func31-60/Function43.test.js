/* Write a real-valued function Ln1(x, ε) (x and ε are real numbers, |x| < 1, ε > 0)
 that returns an approximate value of the function ln(1 + x) defined as follows:

ln(1 + x) = x − x^2/2 + x^3/3 − … + (−1)^n·x^(n+1)/(n+1) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function ln(1 + x) at a given point x for six given ε.*/
const { Ln1 } = require("./Function43");
describe("functia 43", () => {
    describe("Ln1", () => {
        xit("x= 0.65, eps = 0.2", () => {
            const result = Ln1(0.65, 0.2);

            expect(result).toEqual(-0.35);
        });
        xit("???x= -0.85, eps = 0.5", () => {
            const result = Ln1(0.75, 0.2);

            expect(result).toEqual(-0.25);
        });
        xit("eps negative", () => {
            const result = Ln1(4, -2);

            expect(result).toEqual('Negative epsilon error');
        });
        xit("invalid x", () => {
            const result = Ln1(4, 2);

            expect(result).toEqual("Invalid x");
        });
    });
});