/* Write a function Swap(X, I, J) that exchanges the values of items X_I and X_J
 of a list X of real numbers (I and J are input integer parameters, the function returns the None value).
 Having input a list of four real numbers and using three calls of this function,
 sequentially exchange the values of the two first, two last, and two middle items of the given list.
 Output the new values of the list. */
const { swap } = require("./Function31");
describe("functia 31", () => {
    describe("swap", () => {
        let X;

        beforeEach(() => {
            X = [1, 2, 3, 4];
        })
        xit("twoFirst", () => {
            const result = swap(X, 1, 2);

            expect(result).toEqual([2, 1, 3, 4]);
        })
        xit("twoLast", () => {
            const result = swap(X, 3, 4);

            expect(result).toEqual([1, 2, 4, 3]);
        })
        xit("twoMiddle", () => {
            const result = swap(X, 2, 3);

            expect(result).toEqual([1, 3, 2, 4]);
        })
    })
})