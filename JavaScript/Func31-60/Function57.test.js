/* Using the Leng function from the task Func56, write a real-valued function
 Perim(x_A, y_A, x_B, y_B, x_C, y_C) that returns the perimeter of a triangle ABC
  with given coordinates of its vertices (x_A, y_A, x_B, y_B, x_C, y_C are real-valued parameters).
   Using the Perim function, find the perimeters of triangles ABC, ABD, ACD provided that 
   coordinates of points A, B, C, D are given.*/
const { perim } = require("./Function57");
describe("functia 57", () => {
    describe("perim", () => {
        xit("rect-triangle", () => {
            const result = perim(0, 0, 0, 4, 3, 0);

            expect(result).toEqual(12);
        });
        xit("another rect triangle", () => {
            const result = perim(1, 1, 1, 5, 4, 1);

            expect(result).toEqual(12);
        });
        xit("a triangle", () => {
            const result = perim(-1, -1, 2, 2, 3, -2.5);

            expect(result).toEqual(13.124);
        });
        xit("not a triangle", () => {
            const result = perim(1, 1, 1, 1, 4, 1);

            expect(result).toEqual("Not a triangle");
        });
    });
});