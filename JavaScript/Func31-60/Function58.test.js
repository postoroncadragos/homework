/* Using the Leng and Perim functions from the tasks Func56 and Func57, write a real-valued
 function Area(x_A, y_A, x_B, y_B, x_C, y_C) that returns the area of a triangle ABC:

S_ABC = (p·(p−|AB|)·(p−|AC|)·(p−|BC|))^1/2,

where p is the half-perimeter. Using the Area function, find the areas of triangles
 ABC, ABD, ACD provided that coordinates of points A, B, C, D are given. */
const { area } = require("./Function58");
describe("functia 58", () => {
    describe("area", () => {
        xit("rect-triangle", () => {
            const result = area(0, 0, 0, 4, 3, 0);

            expect(result).toEqual(6);
        });
        xit("another rect triangle", () => {
            const result = area(1, 1, 1, 5, 4, 1);

            expect(result).toEqual(6);
        });
        xit("a triangle", () => {
            const result = area(-1, -1, 2, 2, 3, -2.5);

            expect(result).toEqual(8.249);
        });
        xit("not a triangle", () => {
            const result = area(2, 3, 2, 2, 2, 2);

            expect(result).toEqual("Not a triangle");
        });
    });
});