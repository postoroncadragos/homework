/* Write a real-valued function Exp1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function exp(x) defined as follows:

exp(x) = 1 + x + x^2/(2!) + x^3/(3!) + … + x^n/(n!) + …

(n! = 1·2·…·n). Stop adding new terms to the sum when the value of the next term
will be less than ε. Using this function, find the approximate values of the function
exp(x) at a given point x for six given ε. */
const { Power1 } = require("./Function37");
const factorial = (arg) => {
    if (arg === 0) {
        return 1;
    }
    return factorial(arg - 1) * arg;
}
const Exp1 = function (x, eps) {
    if (eps <= 0) {
        return "Negative epsilon error";
    }
    const newTerm = (n) => {
        return Power1(x, n) / factorial(n);
    }
    let res = 1;
    for (let i = 1; Math.abs(newTerm(i)) >= eps; i++) {
        res += newTerm(i);
    }
    const fix3 = (num) => {
        return num.toFixed(3);
    }
    return Number(fix3(res));
}
module.exports = { Exp1, factorial };
