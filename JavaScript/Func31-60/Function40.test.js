/* Write a real-valued function Exp1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function exp(x) defined as follows:

exp(x) = 1 + x + x^2/(2!) + x^3/(3!) + … + x^n/(n!) + …

(n! = 1·2·…·n). Stop adding new terms to the sum when the value of the next term
will be less than ε. Using this function, find the approximate values of the function
exp(x) at a given point x for six given ε. */
const { Exp1 } = require("./Function40");
describe("functia 40", () => {
    describe("Exp1", () => {
        it("x= 2, eps = 0.9", () => {
            const result = Exp1(2, 0.9);

            expect(result).toEqual(6.333);
        });
        it("x= 2.5, eps = 0.9", () => {
            const result = Exp1(2.5, 0.9);

            expect(result).toEqual(10.857);
        });
        xit("x negative", () => {
            const result = Exp1(-3, 2);

            expect(result).toEqual(-0.65);
        });
        xit("eps negative", () => {
            const result = Exp1(4, -2);

            expect(result).toEqual('Negative epsilon error');
        });
    });
});