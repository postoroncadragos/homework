/* Write a real-valued function Cos1(x, ε) (x and ε are real numbers, ε > 0)
 that returns an approximate value of the function cos(x) defined as follows:

cos(x) = 1 − x^2/(2!) + x^4/(4!) − … + (−1)^n·x^(2·n)/((2·n)!) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function cos(x) at a given point x for six given ε. */
const { Power1 } = require("./Function37");
const { factorial } = require("./Function40");
const Cos1 = function (x, eps) {
    if (eps <= 0) {
        return "Negative epsilon error";
    }
    const newTerm = (n) => {
        let sign = Math.pow(-1, n / 2);
        return sign * Power1(x, n) / factorial(n);
    }
    let res = 0;
    for (let i = 0; Math.abs(newTerm(i)) >= eps; i += 2) {

        res += newTerm(i);
    }
    const fix3 = (num) => {
        return num.toFixed(3);
    }
    return Number(fix3(res));
}
module.exports = { Cos1 };
