/* Write a real-valued function Ln1(x, ε) (x and ε are real numbers, |x| < 1, ε > 0)
 that returns an approximate value of the function ln(1 + x) defined as follows:

ln(1 + x) = x − x^2/2 + x^3/3 − … + (−1)^n·x^(n+1)/(n+1) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function ln(1 + x) at a given point x for six given ε.*/
const { Power1 } = require("./Function37");
const Ln1 = function (x, eps) {
    if (eps <= 0) {
        return "Negative epsilon error";
    }
    if (Math.abs(x) >= 1) {
        return "Invalid x";
    }
    const newTerm = (n) => {
        let sign = Math.pow(-1, n + 1);
        return sign * Power1((x - 1), n) / n;
    }
    let res = 0;
    for (let i = 1; Math.abs(newTerm(i)) >= eps; i++) {
        res += newTerm(i);
    }
    const fix3 = (num) => {
        return num.toFixed(3);
    }
    return Number(fix3(res));
}
module.exports = { Ln1 };
