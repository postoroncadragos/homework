/* Using the Power1 and Power2 functions (see Func37 and Func38), write a real-valued function
 Power3(A, B) that returns the power A^B calculated as follows (A and B are real-valued parameters):
  if B has a zero-valued fractional part then the function Power2(A, N) is called (an integer variable N is equal to B),
   otherwise the function Power1(A, B) is called. Having input real numbers P, A, B, C and using the Power3 function,
    find the powers A^P, B^P, C^P. */
const { Power2 } = require("./Function38");
const { Power1 } = require("./Function37");
const Power3 = function (A, B) {
    if (Number.isInteger(B)) {
        return Power2(A, B);
    }
    return Power1(A, B);
}
module.exports = { Power3 };
