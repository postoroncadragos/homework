/* Taking into account that the least common multiple of two positive integers A and B equals A·(B/GCD(A, B)),
 where GCD(A, B) is the greatest common divisor of A and B, and using the GCD2 function from the task Func46,
  write an integer function LCM2(A, B) that returns the least common multiple of A and B. Using this function,
   find the least common multiple for each of pairs (A, B), (A, C), (A, D) provided that integers A, B, C, D are given. */
const { lcm2 } = require("./Function48");
describe("functia 48", () => {
    describe("lcm2", () => {
        xit("a si b >0", () => {
            const result = lcm2(36, 24);

            expect(result).toEqual(72);
        });
        xit("a si b >0(2)", () => {
            const result = lcm2(12, 69);

            expect(result).toEqual(276);
        });
        xit("b===0", () => {
            const result = lcm2(48, 0);

            expect(result).toEqual("Nedefinit!");
        });
        xit("a===0", () => {
            const result = lcm2(0, 21);

            expect(result).toEqual("Nedefinit!");
        });
        xit("a and b are negative", () => {
            const result = lcm2(-2, -4);

            expect(result).toEqual("Not positive integers!");
        });
        xit("a is negative", () => {
            const result = lcm2(-23, 34);

            expect(result).toEqual("Not positive integers!");
        });
        xit("b is negative", () => {
            const result = lcm2(2, -54);

            expect(result).toEqual("Not positive integers!");
        });
    });
});