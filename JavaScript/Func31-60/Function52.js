/* Write a logical function IsLeapYear(Y) that returns True if a year Y 
(a positive integer parameter) is a leap year, and False otherwise.
 Output the return values of this function for five given values of the parameter Y.
  Note that a year is a leap year if it is divisible by 4 except for years that are 
  divisible by 100 and are not divisible by 400 */
const isLeapYear = (y) => {
    if (y % 100 === 0) {
        y /= 100;
    }
    let myBool = (y % 4 === 0);
    return myBool;
}
module.exports = { isLeapYear };