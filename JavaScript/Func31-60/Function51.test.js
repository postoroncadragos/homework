/* Write a function IncTime(H, M, S, T) that increases a time interval in hours H,
 minutes M, seconds S on T seconds and returns new values of hours, minutes,
  and seconds (all numbers are positive integers). Having input hours H, minutes M,
   seconds S (as integers) and an integer T and using the IncTime function,
    increase the given time interval on T seconds and output new values of H, M, S. */
const { incTime } = require("./Function51");
describe("functia 48", () => {
    describe("incTime", () => {
        xit("valid", () => {
            const result = incTime(1, 36, 20, 24);

            expect(result).toEqual({ hours: 1, minutes: 36, seconds: 44 });
        });
        xit("valid(2)", () => {
            const result = incTime(1, 56, 20, 40);

            expect(result).toEqual({ hours: 1, minutes: 57, seconds: 0 });
        });
        xit("valid(3)", () => {
            const result = incTime(3, 50, 20, 580);

            expect(result).toEqual({ hours: 4, minutes: 0, seconds: 0 });
        });
        xit("case of negative values", () => {
            const result = incTime(2, 45, 30, -32);

            expect(result).toEqual("Give positive values!");
        });
    });
});