/* Write a real-valued function Atan1(x, ε) (x and ε are real numbers, |x| < 1, ε > 0)
 that returns an approximate value of the function atan(x) defined as follows:

atan(x) = x − x^3/3 + x^5/5 − … + (−1)^n·x^(2·n+1)/(2·n+1) + … .

Stop adding new terms to the sum when the absolute value of the next term will be less than ε.
 Using this function, find the approximate values of the function atan(x) at a given point x for six given ε. */
const { Power1 } = require("./Function37");
const Atan = function (x, eps) {
    if (eps <= 0) {
        return "Negative epsilon error";
    }
    if (Math.abs(x) >= 1) {
        return "Invalid x";
    }
    const newTerm = (n) => {
        let sign = Math.pow(-1, n);
        return sign * Power1(x, 2 * n + 1) / (2 * n + 1);
    }
    let res = 0;
    for (let i = 0; Math.abs(newTerm(i)) >= eps; i++) {
        res += newTerm(i);
    }
    const fix3 = (num) => {
        return num.toFixed(3);
    }
    return Number(fix3(res));
}
module.exports = { Atan };
