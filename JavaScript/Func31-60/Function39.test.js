/* Using the Power1 and Power2 functions (see Func37 and Func38), write a real-valued function
 Power3(A, B) that returns the power A^B calculated as follows (A and B are real-valued parameters):
  if B has a zero-valued fractional part then the function Power2(A, N) is called (an integer variable N is equal to B),
   otherwise the function Power1(A, B) is called. Having input real numbers P, A, B, C and using the Power3 function,
    find the powers A^P, B^P, C^P. */
const { Power3 } = require("./Function39");
describe("functia 39", () => {
    describe("Power3", () => {
        xit("b not integer", () => {
            const result = Power3(4, 2.5);

            expect(result).toEqual(32);
        });
        xit("b  integer", () => {
            const result = Power3(4, -2);

            expect(result).toEqual(0.0625);
        });
        xit("b not integer2", () => {
            const result = Power3(25, 0.5);

            expect(result).toEqual(5);
        });
        xit("b  integer2", () => {
            const result = Power3(0.5, -2);

            expect(result).toEqual(4);
        });
    });
});