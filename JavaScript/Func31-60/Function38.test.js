/* Write a real-valued function Power2(A, N) that returns the power A^N calculated by the following formulas:
A^0 = 1;
A^N = A·A·…·A (N factors), if N > 0;
A^N = 1/(A·A·…·A) (|N| factors), if N < 0
(A is a real-valued parameter, N is an integer parameter). Having input a real number A and integers
 K, L, M and using this function, find the powers A^K, A^L, A^M. */
const { Power2 } = require("./Function38");
describe("functia 38", () => {
    describe("Power2", () => {
        xit("Not integer", () => {
            const result = Power2(2, 4.4);

            expect(result).toEqual("Give an integer value!");
        });
        xit("zero", () => {
            const result = Power2(3, 0);

            expect(result).toEqual(1);
        });
        xit("valid  numbers", () => {
            const result = Power2(2, 4);

            expect(result).toEqual(16);
        });
        xit("N negative", () => {
            const result = Power2(2, -2);

            expect(result).toEqual(0.25);
        });
        xit("valid  numbers2", () => {
            const result = Power2(5, 3);

            expect(result).toEqual(125);
        });
        xit("N negative2", () => {
            const result = Power2(0.5, -2);

            expect(result).toEqual(4);
        });
    });
});