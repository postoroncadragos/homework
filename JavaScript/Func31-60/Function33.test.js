/*Write a function SortInc3(X) that sorts the list X of three real-valued items
 in ascending order (the function returns the None value). Using this function,
  sort each of two given lists X and Y. */
const { SortInc3 } = require("./Function33");
describe("functia 33", () => {
    describe("SortInc3", () => {
        xit("Sort list X", () => {
            const list = [3, 6, 4]

            const result = SortInc3(list);

            expect(result).toEqual([3, 4, 6]);
        });
        xit("Sort list Y", () => {
            const list = [45, -56, -2]

            const result = SortInc3(list);

            expect(result).toEqual([-56, -2, 45]);
        });
    });
});
