/* Write a function ShiftRight3(X) that performs a right cyclic shift of a list X 
of three real-valued items: the value of each item should be assigned to the next 
item and the value of the last item should be assigned to the first item (the function 
returns the None value). Using this function, perform the right cyclic shift for each
 of two given lists X and Y. */
const ShiftRight3 = function (X) {
    let last = X.pop();
    X.unshift(last);
    return X;
}
module.exports = { ShiftRight3 };
