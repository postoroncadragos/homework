/* Using the IsLeapYear function from the task Func52, write an integer function MonthDays(M, Y)
 that returns the amount of days for M-th month of year Y (M and Y are integers, 1 ≤ M ≤ 12, Y > 0).
  Output the return value of this function for a given year Y and each of given months M_1, M_2, M_3. */
const { isLeapYear } = require("./Function52");
const monthDays = (m, y) => {
    if (m < 1 || m > 12) {
        return "Invalid number of month"
    }
    if (y <= 0) {
        return "Invalid number of year"
    }
    let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (isLeapYear(y) && m === 2) {
        return 29;
    }
    return daysInMonth[m - 1];
}
module.exports = { monthDays };