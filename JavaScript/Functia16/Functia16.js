/*Write a logical function IsPalindrome(K) that returns True, if the decimal representation of a positive 
parameter K is a palindrome (i. e., it is read equally both from left to right and from right to left), 
and False otherwise. Using this function, find the amount of palindromes in a given sequence of 10 positive integers.
 */
const IsPalindrome = function (K) {
    if (isNaN(K)) {
        return false;
    };
    const str = K.toString();
    const reverseStr = str.split("").reverse().join("")
    
    return str === reverseStr;
}
const solve = function () {
    function giveMeTheNumber(string) {
        return string.split(",")
            .map(x => { return IsPalindrome(x) ? 1 : 0 })
            .reduce((a, b) => { return a + b });
    }
    let a = document.forma.sequence.value;
    let res = "Ai introdus " + giveMeTheNumber(a) + " numere palindrom ";
    document.getElementById("result").innerHTML = res;

}
