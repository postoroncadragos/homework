/* Write a logical function IsPrime(N) that returns True, if an integer parameter N (> 1) is a prime number,
       and False otherwise. Using this function, find the amount of prime numbers in a given sequence of 10 integers greater than 1.
        Note that an integer (> 1) is called a prime number if it has not positive divisors except 1 and itself.
*/
function IsPrime(N) {
  for (let i = 2; i <= Math.sqrt(N); i++) {
    if (N % i === 0) {
      return false;
    }
  }
  return true;
}

const solve = function () {
  function giveMeTheNumber(string) {
    return string.split(",")
      .map(x => { return IsPrime(x) ? 1 : 0 })
      .reduce((a, b) => { return a + b });
  }
  let a = document.forma.a.value;
  let res = "Ai introdus " + giveMeTheNumber(a) + " numere prime ";
  document.getElementById("result").innerHTML = res;
}