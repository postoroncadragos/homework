/*Write a function PowerA3(A) that returns the third degree of a real number A (A is an input parameter). Using
            this function, find the third degree of five given real numbers.
 */
const PowerA3 = function (A) {
    let result = Math.pow(A, 3);
    return result;
}


const solve = function () {
    let a = document.forma.sequence.value;
    let noCommas = a.replace(/,/g, ".");
    let res = noCommas.split(";").map(x => { return "Puterea a treia pentru " + x + " = " + PowerA3(x) + " " })
    document.getElementById("result").innerHTML = res;
}
