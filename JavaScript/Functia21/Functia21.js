/*Write an integer function Fib(N) that returns the value of N-th term of the sequence of the Fibonacci numbers.
 The Fibonacci numbers FK are defined as follows:
F1 = 1, F2 = 1, FK = FK−2 + FK−1, K = 3, 4, … .
Using this function, find five Fibonacci numbers with given order numbers N1, N2, …, N5.
 */
const Fib = function () {
    const memo = [1, 1]
    return (n) => {
        if (memo.length >= n) {
            return memo[n-1]
        }
        console.log(`calculate from ${memo[memo.length - 1]}(index ${memo.length - 1}) to index ${n}`)
        let fibValue = memo[memo.length - 1]
        for (let i = memo.length; i < n; i++){
            fibValue = memo[i - 2] + fibValue
            memo.push(fibValue)
        }

        return fibValue
    }
}


const solve = function () {
    let a = document.forma.sequence.value;
    let res = a.split(",").map(x => { return " Fibbonacci(" + x + ")=" + Fib(x) + " " })
    document.getElementById("result").innerHTML = res;
}
