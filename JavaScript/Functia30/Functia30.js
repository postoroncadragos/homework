/* Write a function AddLeftDigit(D, K) that adds a digit D to the left side of the decimal representation of a
  positive integer K and returns the obtained number (D and K are input integer parameters, the value of D is
  in the range 0 to 9). Having input an integer K and two one-digit numbers D1, D2 and using two calls of this
  function, sequentially add the given digits D1, D2 to the left side of the given K and output the result of
  each adding. */
const AddLeftDigit = function (D, K) {
  if (K < 0) {
    return "Not a positive integer;"
  }
  let count = 1;
  let number = K;
  while (number >= 10) {
    number = Math.floor(number / 10);
    count++;
  }
  let result = D * Math.pow(10, count) + parseInt(K);
  return result;
}

const solve = function () {
  let D = document.forma.digit.value;
  let K = document.forma.number.value;
  let res = AddLeftDigit(D, K);

  document.getElementById("num").value = res;
}