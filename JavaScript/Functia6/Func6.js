/* Write an integer function SumRange(A, B) that returns a sum of all integers in the range A to B inclusively 
        (A and B are integers). In the case of A > B the function returns 0. Using this function, find a sum of all
         integers in the range A to B and in the range B to C provided that integers A, B, C are given.*/
const SumRange = function (A, B) {
    if (A > B) {
        return 0;
    }
    let sumAB = 0;
    if (A <= B) {
        for (let i = A; i <= B; i++) {
            sumAB += parseInt(i);
        }
        return "Suma cifrelor de la " + A + " pina la " + B + " este:  " + sumAB;
    }
}
const solve = function () {
    let a = document.forma.A.value;
    let b = document.forma.B.value;
    let c = document.forma.C.value;
    document.getElementById("Intervalul1").innerHTML = SumRange(a, b);
    document.getElementById("Intervalul2").innerHTML = SumRange(b, c);
}

