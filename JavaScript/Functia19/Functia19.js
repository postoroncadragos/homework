/*Func19. Write a real-valued function Fact(N) that returns a factorial of a positive integer N: N! = 1·2·…·N 
(the real return type allows to avoid the integer overflow during the calculation of the factorials for large values
     of the parameter N). Using this function, find the factorials of five given integers.
 */
const Fact = function (N) {
    let res = 1;
    for (let i = 1; i <= N; i++) {
        res *= parseInt(i);
    }
    return res;
}


const solve = function () {
    let a = document.forma.sequence.value;
    let res = a.split(",").map(x => { return "Factorial of " + x + "=" + Fact(x) + " " })
    document.getElementById("result").innerHTML = res;
}
