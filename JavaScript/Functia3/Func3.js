/* Write a real-valued function CircleS(R) that returns the area of a circle of radius R (R is a real number).
         Using this function, find the areas of three circles of given radiuses. Note that the area of a circle of
          radius R can be found by formula S = π·R^2. Use 3.14 for a value of π.*/
const CircleS = function (R) {
    return Math.PI * Math.pow(R, 2);
}
const solve = function () {
    let r = document.forma.raza1.value;
    document.getElementById("aria").innerHTML = CircleS(r);
}
