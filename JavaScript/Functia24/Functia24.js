/* Write a function Mean(X, Y) that computes the arithmetical mean (X+Y)/2 and the geometrical mean (X·Y)^1/2 of
    two positive real numbers X and Y and returns the result as two real numbers (X and Y are input parameters).
    Using this function, find the arithmetical mean and the geometrical mean of pairs (A, B), (A, C), (A, D)
    provided that real numbers A, B, C, D are given.
 */
const Mean = function (X, Y) {
    let res = [];
    let arithmeticalMean = (parseInt(X) + parseInt(Y)) / 2;
    let geometricalMean = Math.sqrt(X * Y);
    res[0] = arithmeticalMean;
    res[1] = geometricalMean;
    return res;
}

const solve = function () {
    let A = document.forma.A.value;
    let seq = document.forma.sequence.value;
    let noCommas = seq.replace(/,/g, ".");

    function modifiedResult(arg1, arg2) {
        let ourFunc = Mean(arg1, arg2);
        let string = " media"
        let arrStrings = [" aritmetica", " geometrica",]
        let res = "";
        for (let i = 0; i < ourFunc.length; i++) {
            res += string + arrStrings[i] + " = " + Mean(arg1, arg2)[i] + " ; ";
        }
        return res;
    }
    let res = noCommas.split(";").map((x) => { return "Pentru " + A + " si " + x + modifiedResult(A, x) + " " }).join("");
    document.getElementById("result").innerHTML = res;
}

