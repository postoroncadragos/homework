/* Write a function DigitCS(K) that finds and returns the amount C of digits in the decimal representation of a
    positive integer K and also the sum S of its digits (K is an input integer parameter). Using this function,
    find the amount and the sum of digits for each of five given integers.
 */
const DigitCS = function (K) {
    if (K < 0) {
        return "";
    }
    let C = 0;
    let S = 0;
    while (K >= 1) {
        S += K % 10;
        C++;
        K = Math.floor(K / 10);
    }
    let result = [];
    result[0] = C;
    result[1] = S;
    return result;
}

const solve = function () {
    let number = document.forma.number.value;
    let count = DigitCS(number)[0];
    let sum = DigitCS(number)[1];
    let res = number + " este format din " + count + " cifre care adunate fac: " + sum;
    document.getElementById("result").innerHTML = res;
}
