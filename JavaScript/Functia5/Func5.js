/* Write a real-valued function TriangleP(a, h) that returns the perimeter of an isosceles 
        triangle with given base a and altitude h (a and h are real numbers). Using this function,
         find the perimeters of three triangles with given bases and altitudes. Note that the leg b 
         of an isosceles triangle can be found by the Pythagorean theorem:
        b^2 = (a/2)^2 + h^2. */
var TriangleP = function (baza, h) {
    if (baza < 0 || h < 0) {
        return "Triunghi inexistent";
    }
    let laturaCongruenta = Math.sqrt(Math.pow(baza / 2, 2) + Math.pow(h, 2));
    let perimetru = parseInt(baza) + 2 * laturaCongruenta;
    return perimetru;
}
var solve = function () {
    let a = document.forma.a.value;
    let h = document.forma.h.value;
    document.getElementById("p1").innerHTML = TriangleP(a, h);
}
