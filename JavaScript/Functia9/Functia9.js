/* Write a logical function Even(K) that returns True, if an integer parameter K is an even number, and False otherwise.
 Using this function, find the amount of even numbers in a given sequence of 10 integers.*/
const Even = function (K) {
  return K % 2 == 0;
}

const solve = function () {
  let a = document.forma.a.value;
  let res;
  function giveMeTheNumber(string) {
    return string.split(",")
      .map(x => { return Even(parseInt(x)) ? 1 : 0 })
      .reduce((a, b) => { return a + b })
  }
  res = "Ai introdus " + giveMeTheNumber(a) + " numere pare";
  document.getElementById("result").innerHTML = res;
}