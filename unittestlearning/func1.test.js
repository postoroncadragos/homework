const { sign } = require("./func1");
describe("func1", () => {
    describe("sign", () => {
        it("x>0", () => {
            const result = sign(2)

            expect(result).toEqual(1)
        })
        it("x==0", () => {
            const result = sign(0)

            expect(result).toEqual(0)
        })
        it("x<0", () => {
            const result = sign(-2)

            expect(result).toEqual(-1)
        })
    })
})